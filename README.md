# `clinterfaced`

This is an example package which uses the [`clinterfacer`](https://gitlab.com/adrianohrl/clinterfacer) framework to create a command-line interface named as `clinterfaced`.

## Instalation

Using [`pipenv`](https://pipenv-fork.readthedocs.io/), enter the following commands in the terminal:

```bash
git clone git@gitlab.com:adrianohrl/clinterfaced.git
cd clinterfaced
pipenv install
```

## Usage

After installation, enter the following command in terminal:

```bash
pipenv run clinterfaced --help
```

## Documentation

*Coming soon!!!*