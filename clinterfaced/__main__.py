#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This is the main script of the clinterfaced package.
"""

# standard library(ies)
import sys

# 3rd party package(s)
import clinterfacer


def run() -> int:
    cli = clinterfacer.CLI()
    return cli.main()


if __name__ == '__main__':
    sys.exit(run())
