#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""This is the main script of the clinterfaced's get-param command."""


# standard library(ies)
import argparse
import logging
import sys
import time

# local source(s)
from clinterfaced.subparsers.get_param import parse_args


logger = logging.getLogger(__name__)
parameters = {
    'name': 'CLInterfacer',
    'occupation': 'Python Developer',
}


def run(args: argparse.Namespace) -> None:
    logger.info(f'Getting parameter the clinterfaced\'s {args.parameter} paramenter ...')
    logger.info(f'Value for the {args.parameter} parameter: \'{parameters[args.parameter]}\'')


if __name__ == '__main__':
    start = time.time()
    args = parse_args()
    sys.exit(main(args))
    elapsed_time = (time.time() - start) / 60
    logger.info(f'Elapsed: {elapsed_time:.2f} minutes')
