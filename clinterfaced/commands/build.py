#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""This is the main script of the clinterfaced's build command."""


# standard library(ies)
import argparse
import logging
import sys
import time

# local source(s)
from clinterfaced import utils
from clinterfaced.subparsers.build import parse_args


logger = logging.getLogger(__name__)


def run(args: argparse.Namespace) -> None:
    logger.info('Building clinterfaced...')
    logger.debug('Verbose!!!')
    utils.build()
    logger.info('Built clinterfaced')


if __name__ == '__main__':
    start = time.time()
    args = parse_args()
    sys.exit(main(args))
    elapsed_time = (time.time() - start) / 60
    logger.info(f'Elapsed: {elapsed_time:.2f} minutes')
