#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""This is the main script of the clinterfaced's config command."""


# standard library(ies)
import argparse
import logging
import sys
import time

# local source(s)
from clinterfaced import utils
from clinterfaced.subparsers.config import parse_args


logger = logging.getLogger(__name__)


def run(args: argparse.Namespace) -> None:
    logger.info('Configuring clinterfaced...')
    logger.debug('Not quiet!!!')
    utils.setup(args.filename)
    logger.info('Setting up clinterfaced...')


if __name__ == '__main__':
    start = time.time()
    args = parse_args()
    sys.exit(main(args))
    elapsed = (time.time() - start) / 60
    logger.info(f'Elapsed: {elapsed:.2f} minutes')
