#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""This is the main script of the clinterfaced's doc command."""


# standard library(ies)
import argparse
import logging
import os
import sys
import time

# local source(s)
from clinterfaced.subparsers.doc import parse_args


logger = logging.getLogger(__name__)


def build(builder: str) -> None:
    os.system(f'sphinx-build -b {builder} ./docs/{builder}/ ./docs/_build/')


def run(args: argparse.Namespace) -> None:
    logger.info('Generating clinterfaced\'s documentation')
    build(args.builder)


if __name__ == '__main__':
    start = time.time()
    args = parse_args()
    sys.exit(main(args))
    elapsed_time = (time.time() - start) / 60
    logger.info(f'Elapsed: {elapsed_time:.2f} minutes')
