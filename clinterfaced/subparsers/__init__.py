#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""This script contain the add_arguments function for cli package."""


# standard library(ies)
import argparse


def add_arguments(parser: argparse.ArgumentParser):
    parser.add_argument(
        '-o',
        '--output',
        type=str,
        action='store',
        help='Path to the output file.',
        required=False,
    )