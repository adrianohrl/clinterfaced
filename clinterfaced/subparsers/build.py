#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""This script defines the clinterfaced's build subparser."""


# standard library(ies)
import argparse

# 3rd party package(s)
from clinterfacer import cli


def add_parser(subparsers: argparse._SubParsersAction) -> None:
    parser = subparsers.add_parser(
        'build',
        help='This is the build command of the clinterfaced package',
        prog='clinterfaced build',
        description='This command is the build one.',
    )
    add_arguments(parser)


def add_arguments(parser: argparse.ArgumentParser) -> None:
    pass


def parse_args() -> argparse.Namespace: 
    parser = argparse.ArgumentParser()
    add_arguments(parser)
    args = parser.parse_args()
    cli.setup()
    return args