#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""This script defines the clinterfaced's train subparser."""


# standard library(ies)
import argparse

# 3rd party package(s)
from clinterfacer import cli


def add_parser(subparsers: argparse._SubParsersAction) -> None:
    parser = subparsers.add_parser(
        'train',
        help='This is the train command of the clinterfaced package',
        prog='clinterfaced train',
        description='This command is the train one.',
    )
    add_arguments(parser)


def add_arguments(parser: argparse.ArgumentParser) -> None:
    pass

    
def parse_args() -> argparse.Namespace: 
    parser = argparse.ArgumentParser()
    add_arguments(parser)
    args = parser.parse_args()
    cli.setup()
    return args