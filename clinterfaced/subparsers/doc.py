#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""This script defines the clinterfaced's doc subparser."""


# standard library(ies)
import argparse

# 3rd party package(s)
from clinterfacer import cli


def add_parser(subparsers: argparse._SubParsersAction) -> None:
    parser = subparsers.add_parser(
        'doc',
        help='This is the doc command of the clinterfaced package',
        prog='clinterfaced doc',
        description='This command is the doc one.',
    )
    add_arguments(parser)


def add_arguments(parser: argparse.ArgumentParser) -> None:
    parser.add_argument(
        '-b',
        '--builder',
        type=str,
        action='store',
        help='Specify the sphinx builder.',
        required=True,
    )
    

def parse_args() -> argparse.Namespace: 
    parser = argparse.ArgumentParser()
    add_arguments(parser)
    args = parser.parse_args()
    cli.setup()
    return args
