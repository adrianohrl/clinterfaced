#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""This script defines the clinterfaced's get-param subparser."""

# standard library(ies)
import argparse
import os

# 3rd party package(s)
from clinterfacer import cli


def add_parser(subparsers: argparse._SubParsersAction) -> None:
    parser = subparsers.add_parser(
        'get-param',
        help='This is the get-param command of the clinterfaced package',
        prog='clinterfaced get-param',
        description='This command is the get-param one.',
    )
    add_arguments(parser)


def add_arguments(parser: argparse.ArgumentParser) -> None:
    parser.add_argument(
        'parameter',
        help='Specifies the name of the desired paramenter.',
        metavar='PARAMETER',
        default=os.environ.get('CLINTERFACED_GET_PARAM_PARAMETER', 'name'),
    )


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    add_arguments(parser)
    args = parser.parse_args()
    cli.setup()
    return args
