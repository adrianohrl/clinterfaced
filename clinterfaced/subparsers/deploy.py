#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""This script defines the clinterfaced's config subparser."""


# standard library(ies)
import argparse

# 3rd party package(s)
from clinterfacer import cli

# local source(s)
from clinterfacer.utils import types


def add_parser(subparsers: argparse._SubParsersAction) -> None:
    parser = subparsers.add_parser(
        'deploy',
        help='This is the deploy command of the clinterfaced package',
        prog='clinterfaced deploy',
        description='This command is the deploy one.',
    )
    add_arguments(parser)
    

def add_arguments(parser: argparse.ArgumentParser) -> None:
    parser.add_argument(
        '-p',
        '--positive-int',
        type=types.positive_int,
        action='store',
        help='Path to the config file.',
        required=True,
        dest='positive_int',
    )
    

def parse_args() -> argparse.Namespace: 
    parser = argparse.ArgumentParser()
    add_arguments(parser)
    args = parser.parse_args()
    cli.setup()
    return args
