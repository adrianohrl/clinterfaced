#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""__init__.py: This script includes all the project's metadata."""


__author__ = [
    'Adriano Henrique Rossette Leite',
]
__maintainer__ = [
    'Adriano Henrique Rossette Leite',
]
__email__ = [
    'adrianohrl@gmail.com',
]
__copyright__ = ''
__credits__ = []
__license__ = ''
__version__ = '4.1.3'
__status__ = 'Development'  # should typically be one of 'Prototype', 'Development',
__description__ = 'This package uses the clinterfacer framework.'
__url__ = 'https://gitlab.com/adrianohrl/clinterfaced'
__author__ = ', '.join(__author__)
__maintainer__ = ', '.join(__maintainer__)
__email__ = ', '.join(__email__)
options = [
    'Development',
    'Prototype',
    'Production',
]
if __status__ not in options:
    raise Exception(
        f'Invalid __status__: {__status__}. It should typically be one of the following: {options}')
