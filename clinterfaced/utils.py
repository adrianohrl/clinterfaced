#!/usr/bin/env python


"""This script contains helper functions used in clinterfaced."""


# standard library(ies)
import logging
import pathlib
import typing


logger = logging.getLogger(__name__)


def build() -> None:
    logger.info('Built!!!')


def setup(filename: typing.Union[str, pathlib.Path]) -> None:
    logger.info(f'Setting up according to {filename}...')


def deploy() -> None:
    logger.info('Deployed!!!')


def doc(builder: str) -> None:
    logger.info(f'Generating the cli-tester documentation in {builder}')


def train(test_size: float = 0.3) -> None:
    logger.info('Loading dataset...')
    logger.info('Datapreping dataset...')
    logger.info('Feature enginring...')
    logger.info(f'Splitting dataset ({test_size})...')
    logger.info('Training...')
    logger.info('Score: 97.68 %')
    logger.info('Saved classifier in models/the_best_one.pkl')
