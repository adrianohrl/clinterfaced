#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""This file is used to install the clinterfaced package."""

# standard library(ies)
import pathlib as pl
import typing

# 3rd parties
import setuptools

# package modules
import clinterfaced as package


def read(path: typing.Union[str, pl.Path], encoding: str = 'utf-8') -> str:
    if isinstance(path, str):
        path = pl.Path(path)
    return path.read_text(encoding) if path.exists() else ''


metadata = {
    'name': package.__name__,
    'version': package.__version__,
    'author': package.__author__,
    'author_email': package.__email__,
    'maintainer': package.__maintainer__,
    'maintainer_email': package.__email__,
    'description': package.__description__,
    'long_description': read('./README.md'),
    'long_description_content_type': 'text/markdown',
    'url': package.__url__,
    'packages': setuptools.find_packages(),
    'include_package_data': True,
    'install_requires': [
    ],
    'entry_points': {
        'console_scripts': find_console_scripts(entry_point='run'),
    },
    'classifiers': [
        'Programming Language :: Python :: 3',
        'Operating System :: OS Independent',
        'Framework :: Command-line Interface',
    ],
}

if __name__ == '__main__':
    setuptools.setup(**metadata)
